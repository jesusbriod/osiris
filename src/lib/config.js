import path from 'path';

let env = process.env.NODE_ENV || 'development';
let rootPath = path.normalize(__dirname + '/..');

var config = {
	development: {
		port: 3001,
		root: rootPath,
		db: 'mysql://osiris:Axtel$2017.@172.16.1.163:3306/retail',
	},
	testing: {
		port: 3001,
		root: rootPath,
		db: 'mysql://osiris:Axtel$2017.@172.16.1.163:3306/retail',
	},
	production: {
		port: 3001,
		root: rootPath,
		db: 'mysql://osiris:Axtel$2017.@172.16.1.163:3306/retail',
	}
};
module.exports = config[env];
