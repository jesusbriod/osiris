import logger from'morgan';
import cookieParser from'cookie-parser';
import bodyParser from'body-parser';
import compress from'compression';
import methodOverride from'method-override';
import rssiApi from './api/rssi';
import meraki from './api/meraki';
import meraki_history from './api/meraki_history';
import top_products from './api/top_products';
import coupons from './api/coupons';
import moods from './api/moods';
import recommendations from './api/recommendations';
import devices_service from './api/devices_service';
import people_service from './api/people_service';
import products_service from './api/products_service';
import kpi_service from './api/kpi_service';
import barcode from './api/barcode';

export default function (app/*, db*/) {
	//let env = process.env.NODE_ENV || 'development';
	app.use(logger('common'));
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({
		extended: true
	}));
	app.use(cookieParser());
	app.use(compress());
	app.use(methodOverride());

	app.use('/api/meraki', meraki(app.db));
	app.use('/api/meraki_history', meraki_history(app.db));
	app.use('/api/rssi', rssiApi(app.db));
	app.use('/api/top_products', top_products(app.db));
	app.use('/api/coupons', coupons(app.db));
	app.use('/api/moods', moods(app.db));
	app.use('/api/recommendations', recommendations(app.db));
	app.use('/api/devices_service', devices_service(app.db));
	app.use('/api/people_service', people_service(app.db));
	app.use('/api/products_service', products_service(app.db));
	app.use('/api/kpi_service', kpi_service(app.db));
	app.use('/api/barcode', barcode(app.db));

}
