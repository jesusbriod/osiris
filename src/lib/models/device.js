export default function (sequelize, DataTypes) {
	let deviceModel = sequelize.define('device', {
		macUser: {
			type: DataTypes.STRING,
			primaryKey: true,
			field: 'mac_user'
		},
		macRouter: {
			type: DataTypes.STRING,
			primaryKey: true,
			field: 'mac_router'
		},
		rssi: DataTypes.INTEGER,
		seenTime: {
			type: DataTypes.DATE,
			field: 'seen_time'
		} ,
		connected: DataTypes.BOOLEAN
	}, {
		//tableName: 'device'
	});

	return deviceModel;

}
