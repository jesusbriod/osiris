export default function (sequelize, DataTypes) {
	let jaccard_productModel = sequelize.define('jaccard_product', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      field: 'id'
    },

    date: {
      type: DataTypes.INTEGER,
      field: 'date'
    },

    producta: {
      type: DataTypes.INTEGER,
      field: 'producta'
    },

    productb: {
      type: DataTypes.INTEGER,
      field: 'productb'
    },
    relation: {
      type: DataTypes.FLOAT,
      field: 'relation'
    },
  }, {
    timestamp: true,
    createdAt: false,
    updatedAt: false,
    tableName: 'jaccard_products'
  });

	return jaccard_productModel;

}
