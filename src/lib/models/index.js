import fs from 'fs';
import path from 'path';
import Sequelize from 'sequelize';

const tzOffset = () => {
	let x = new Date();
	let o = -x.getTimezoneOffset();
	let s = (o >= 0)? '+' : '';
	let h = parseInt(o/60);
	let r = o%60;
	let m = (r > 9)? '' + r : '0' + r;
	let tz = s + h +":"+ m;
	return tz;
};

export default function (config) {
	let db = {};
	let sequelize = new Sequelize(config.db, {
		omitNull: false,
		timezone: tzOffset()
	});

	fs.readdirSync(__dirname).filter(function (file) {
		return (file.indexOf('.') !== 0) && (file !== 'index.js');
	}).forEach(function (file) {
		var model = sequelize['import'](path.join(__dirname, file));
		db[model.name] = model;
	});

	Object.keys(db).forEach(function (modelName) {
		if ('associate' in db[modelName]) {
			db[modelName].associate(db);
		}
	});

	db.sequelize = sequelize;
	db.Sequelize = Sequelize;

	return db;
}
