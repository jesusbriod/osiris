export default function (sequelize, DataTypes) {
	let history_deviceModel = sequelize.define('history_device', {
		id: {
			type: DataTypes.INTEGER,
			autoIncrement: true,
			primaryKey: true,
			field: 'id'
		},
		macUser: {
			type: DataTypes.STRING,
			field: 'mac_user'
		},
		macRouter: {
			type: DataTypes.STRING,
			field: 'mac_router'
		},
		rssi: DataTypes.INTEGER,
		seenTime: {
			type: DataTypes.DATE,
			field: 'seen_time'
		} ,
		seenDate: {
			type: DataTypes.DATEONLY,
			field: 'seen_date'
		} ,
		os: {
			type: DataTypes.STRING,
			field: 'os'
		} ,
		connected: DataTypes.BOOLEAN
	}, {
		//tableName: 'history_device'
	});

	return history_deviceModel;

}
