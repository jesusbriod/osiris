export default function (sequelize, DataTypes) {
	let productModel = sequelize.define('product', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      field: 'id'
    },

    name: {
      type: DataTypes.STRING,
      field: 'name'
    },

    description: {
      type: DataTypes.STRING,
      field: 'description'
    },

    product_base: {
      type: DataTypes.FLOAT,
      field: 'product_base'
    },

    brand: {
      type: DataTypes.STRING,
      field: 'brand'
    },

    barcode: {
      type: DataTypes.STRING,
      field: 'barcode'
    },

    category: {
      type: DataTypes.STRING,
      field: 'category'
    },

    subcategory: {
      type: DataTypes.STRING,
      field: 'subcategory'
    },

    quantity: {
      type: DataTypes.FLOAT,
      field: 'quantity'
    },


		//tableName: 'cnn_product'
	});

	return productModel;

}
