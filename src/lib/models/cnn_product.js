export default function (sequelize, DataTypes) {
	let cnn_productModel = sequelize.define('cnn_product', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      field: 'id'
    },

    name: {
      type: DataTypes.STRING,
      field: 'name'
    },

    barcode: {
      type: DataTypes.STRING,
      field: 'barcode'
    },


		//tableName: 'cnn_product'
	});

	return cnn_productModel;

}
