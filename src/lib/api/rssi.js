import express from 'express';
import multer from 'multer';
import fs from 'fs';
import csvparse from'csv-parse';

export default function (db) {
	let router = express.Router();

	fs.mkdtemp('/tmp/uploads-', (err, tmpdir) => {
		if (err) throw err;

		const storage = multer.diskStorage({
			destination:
				(req, file, cb) => cb(null, tmpdir),
			filename:
				(req, file, cb) => cb(null, file.fieldname + '-' + Date.now() + '.csv'),
		});
		const upload = multer({ storage: storage });

		router.get('/', (req, res) => {
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.end('Hello World prueba\n');
			})
			.post('/',upload.single('csvdata'),	(req, res) => {
				try {



					const f = req.file;

					fs.createReadStream(f.destination + '/' + f.filename)
						.pipe(csvparse({delimiter: ','}))
						.on('data', function(csvrow) {
							const p = /\+(.+)\+$/g;
							let m = p.exec(csvrow[2]);
							let rssiValue = m[1];

							db.device.findOrCreate({
								where: {macUser: csvrow[0], macRouter: csvrow[1]},
								defaults: {rssi: rssiValue,
									seenTime: new Date(Date.parse(csvrow[3])),
									connected: true }
							})
							.spread((device, created) => {
								if(!created){
									return device.update({
										rssi: rssiValue,
										updated: new Date(Date.parse(csvrow[3]))
									});
								}
							})
							.error((err) => {
								console.err("Error: %s", err);
							});
						})
						.on('end',function() {
							//do something wiht csvData
							//console.log(csvData);
						});
					res.sendStatus(200);




				} catch (err) {
					console.error("Error %s", err);
					res.sendStatus(400);
				}
			});
	});

	return router;
}
