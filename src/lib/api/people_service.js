import express from 'express';
export default function(){
	let router = express.Router();
	router.get('/people_by_age', (req, res) => {
		var mysql      = require('mysql');
		var connection = mysql.createConnection({
			host     : '172.16.1.163',
			user     : 'osiris',
			password : 'Axtel$2017.',
			database : 'retail'
		});
		connection.connect();
		connection.query('select count(*) cantidad, edad from people_track where DATE(date)>DATE_SUB(CURDATE(), INTERVAL 7 DAY) GROUP BY edad;', function (error, results) {
			if (error) throw error;

			let queryTest = results;
			console.error(typeof(queryTest));
			queryTest.forEach(function(value){
				console.error(typeof(value));
				console.error('The solution is: ', value);
			});
			res.setHeader('Content-Type', 'application/json');

			let jsonArr = {};
			jsonArr.peopleByAge = queryTest;

			let peopleByAgeDays = [];
			let peopleByAgeQuantity = [];
			jsonArr.peopleByAge.forEach(function(peopleByAge){

				console.error('Test: ', peopleByAge);

				peopleByAgeQuantity.push(peopleByAge.cantidad);

				console.error(peopleByAgeDays);
				console.error(peopleByAgeQuantity);

			});

			let peopleByAgeService = {};
			peopleByAgeDays=['1-17','18-24','25-34','35-44','45-54','55+'];
			peopleByAgeService.data= {
				categories : peopleByAgeDays,
				series : [
					{"name":"1-17", "value":peopleByAgeQuantity[0]},
					{"name":"18-24", "value":peopleByAgeQuantity[1]},
					{"name":"25-34", "value":peopleByAgeQuantity[2]},
					{"name":"35-44", "value":peopleByAgeQuantity[3]},
					{"name":"45-54", "value":peopleByAgeQuantity[4]},
					{"name":"55+", "value":peopleByAgeQuantity[5]}
				]
			};



			console.log(peopleByAgeService.data.categories);

			res.send(JSON.stringify(peopleByAgeService));
		});
	})
	.get('/people_by_age_gender', (req, res) => {
		var mysql      = require('mysql');
		var connection = mysql.createConnection({
			host     : '172.16.1.163',
			user     : 'osiris',
			password : 'Axtel$2017.',
			database : 'retail'
		});

		connection.connect();
		connection.query('select count(*) cantidad, edad, genero from people_track where DATE(date)>=DATE_SUB(NOW(), INTERVAL 24 HOUR) GROUP BY edad, genero;', function (error, results) {
			if (error) throw error;
			//let queryTest = JSON.parse(JSON.stringify(results));
			let queryTest = results;
			console.error(typeof(queryTest));
			queryTest.forEach(function(value){
				console.error(typeof(value));
				console.error('The solution is: ', value);
			});
			console.error('The JSON is: ', queryTest);
			res.setHeader('Content-Type', 'application/json');

			let jsonArr = {};
			jsonArr.peopleByWeek = queryTest;

			let peopleByWeekQuantityM = [];
			let peopleByWeekQuantityF = [];
			let peopleByWeekCategories= [];
			jsonArr.peopleByWeek.forEach(function(peopleByWeek){

				if(peopleByWeek.genero==0){


					peopleByWeekQuantityF.push(peopleByWeek.cantidad);
				}

				if(peopleByWeek.genero==1){
					switch(peopleByWeek.edad)
					{
						case 0:
						peopleByWeekCategories.push('1-17');
						break;
						case 1:
						peopleByWeekCategories.push('18-24');
						break;
						case 2:
						peopleByWeekCategories.push('25-34');
						break;
						case 3:
						peopleByWeekCategories.push('35-44');
						break;
						case 4:
						peopleByWeekCategories.push('45-54');
						break;
						case 5:
						peopleByWeekCategories.push('55+');
						break;
						default:
						peopleByWeekCategories.push(' ');
					}

					peopleByWeekQuantityM.push(peopleByWeek.cantidad);
				}

			});
			let peopleByWeekService = {};


		peopleByWeekService.data= {
			categories: peopleByWeekCategories,
			series :[
				{"name":"Mujeres", "value":peopleByWeekQuantityF},
				{"name":"Hombres", "value":peopleByWeekQuantityM}
			]
		};


res.send(peopleByWeekService);

});


})

.get('/people_by_week', (req, res) => {
	var mysql      = require('mysql');
	var connection = mysql.createConnection({
		host     : '172.16.1.163',
		user     : 'osiris',
		password : 'Axtel$2017.',
		database : 'retail'
	});

	connection.connect();
	connection.query('select DAYNAME(date) dia, count(*) cantidad from people_track where DATE(date)>DATE_SUB(CURDATE(), INTERVAL 7 DAY) GROUP BY DAY(date);', function (error, results) {
		if (error) throw error;
		//let queryTest = JSON.parse(JSON.stringify(results));
		let queryTest = results;
		console.error(typeof(queryTest));
		queryTest.forEach(function(value){
			console.error(typeof(value));
			console.error('The solution is: ', value);
		});
		console.error('The JSON is: ', queryTest);
		res.setHeader('Content-Type', 'application/json');

		let jsonArr = {};
		jsonArr.peopleByWeek = queryTest;


		let peopleByWeekQuantity = [];
		let peopleByWeekCategories= [];
		jsonArr.peopleByWeek.forEach(function(peopleByWeek){

			peopleByWeekQuantity.push(peopleByWeek.cantidad);
			peopleByWeekCategories.push(peopleByWeek.dia);

		});
		let peopleByWeekService = {};


	peopleByWeekService.data= {
		categories: peopleByWeekCategories,
		series :[
			{"name":"Total", "value":peopleByWeekQuantity}
		]
	};


res.send(peopleByWeekService);

});


})

.get('/people_by_gender', (req, res) => {
var mysql      = require('mysql');
var connection = mysql.createConnection({
host     : '172.16.1.163',
user     : 'osiris',
password : 'Axtel$2017.',
database : 'retail'
});
connection.connect();
let queryTest;
let jsonArr = {};

let peopleByGenderMQuantity = [];
let peopleByGenderFQuantity = [];
let peopleByGenderDays = [];
let peopleByGenderMDays = [];
let peopleByGenderQuantity = [];

//let dictF = [];
let dictFJ = [];
let dictM = [];

let peopleByGenderService = {};



connection.query('select HOUR(date) dia , count(*) cantidad from people_track where date>DATE_SUB(NOW(), INTERVAL 24 HOUR) GROUP BY HOUR(date) order by date desc limit 8;', function (error, results) {
if (error) throw error;
queryTest = results;
queryTest.forEach(function(value){
console.error(typeof(value));
console.error('The solution is: ', value);
});

jsonArr.peopleByGenderDays = queryTest;

jsonArr.peopleByGenderDays.forEach(function(peopleByGender){
	peopleByGenderDays.push(peopleByGender.dia);
	peopleByGenderQuantity.push(peopleByGender.cantidad);
});

});
connection.query('select HOUR(date) dia , count(*) cantidad from people_track where date>DATE_SUB(NOW(), INTERVAL 24 HOUR) and genero=1 GROUP BY HOUR(date) order by date desc;', function (error, results) {
if (error) throw error;
queryTest = results;

console.error(typeof(queryTest));
queryTest.forEach(function(value){
console.error(typeof(value));
console.error('The solution is: ', value);
});
jsonArr.peopleByGenderM = queryTest;



jsonArr.peopleByGenderM.forEach(function(peopleByGender){
	peopleByGenderMDays.push(peopleByGender.dia);
	peopleByGenderMQuantity.push(peopleByGender.cantidad);
	dictM.push({key:peopleByGender.dia, value: peopleByGender.cantidad});
});

});

connection.query('select HOUR(date) dia , count(*) cantidad from people_track where date>DATE_SUB(NOW(), INTERVAL 24 HOUR) and genero=0 GROUP BY HOUR(date) order by date desc;', function (error, results) {
if (error) throw error;
queryTest = results;

console.error(typeof(queryTest));
queryTest.forEach(function(value){
console.error(typeof(value));
console.error('The solution is: ', value);
});
jsonArr.peopleByGenderF = queryTest;
/*
for (var i = 0; i < peopleByGenderDays.length; i++) {

	peopleByGenderMQuantity[i]=0;
	peopleByGenderFQuantity[i]=0;
}
*/

jsonArr.peopleByGenderF.forEach(function(peopleByGender){
	peopleByGenderFQuantity.push(peopleByGender.cantidad);
	dictM.push({key:peopleByGender.dia, value: peopleByGender.cantidad});
});

/*
for (var j = 0; j < peopleByGenderDays.length; j++) {

	if (peopleGenderDays[j]==people) {
		peopleByGenderFQuantity[i]=0;
	}

}
*/



peopleByGenderService.data= {
categories : peopleByGenderDays,
series : [
	{"name":"Hombres", "data":peopleByGenderMQuantity},
	{"name":"Mujeres", "data":peopleByGenderFQuantity},
	{"name":"Pruebaf", "data":peopleByGenderDays.length},
	{"name":"PruebaFDefinitiva", "data":dictFJ}
]};

//console.log(deviceByBondService.data.categories);
res.setHeader('Content-Type', 'application/json');
res.send(JSON.stringify(peopleByGenderService));


});



})


.post('/', (req, res) => {
	res.sendStatus(200);
	//console.log(JSON.stringify(req.headers));
	//const Op = Sequelize.Op;

});
return router;
}
