import express from 'express';


export default function(db){

	let router = express.Router();
	const validator = "5f7e2589ea88a8053f5ae3295951fe7a4175e6e1";
	const secret = "Axtel#2017$";

	router.get('/', (req, res) => {
			console.log("Send validator: %s", validator);
			res.send(validator);
		})
		.post('/', (req, res) => {
			//console.log(JSON.stringify(req.headers));
			try{
				let jsoned = req.body;
				//console.log("%s == %s", secret, jsoned.secret);
				if(secret === jsoned.secret){
					let ap = jsoned.data;
					let e = 0;
					console.log("AP Mac %s", ap.apMac);
					for(let obs of ap.observations){
						console.log("Observations %o", obs);
						db.device.findOrCreate({
							where: {macUser: obs.clientMac, macRouter: ap.apMac},
							defaults: {rssi: obs.rssi,
								seenTime: new Date(Date.parse(obs.seenTime)),
								connected: (obs.ipv4)? true: false }
						})
						.spread((device, created) => {
							if(!created){
								return device.update({
									rssi: obs.rssi,
									seenTime: new Date(Date.parse(obs.seenTime)),
									connected: (obs.ipv4)? true : false
								});
							}
						})
						.error((err) => {
							if(err) e++;
							//console.err("Error: %s", err);
						});
					}
					console.log("Errores %d", e);
					res.sendStatus(200);
				} else {
					res.sendStatus(403);
				}

			}catch(err){
				console.error("Error %o", err);
				res.sendStatus(400);
			}
		});
	return router;
}
