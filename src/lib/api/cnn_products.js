import express from 'express';
import multer from 'multer';
import fs from 'fs';
//import shelljs from 'shelljs';
//import csvparse from'csv-parse';

export default function (/*db*/) {
	let router = express.Router();


	fs.mkdtemp('/tmp/uploads-', (err, tmpdir) => {
		if (err) throw err;

		const storage = multer.diskStorage({
			destination:
				(req, file, cb) => cb(null, tmpdir),
			filename:
				(req, file, cb) => cb(null, file.fieldname + '-' + Date.now() + '.jpg'),
		});
		const upload = multer({ storage: storage });

		router.get('/', (req, res) => {



				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.end('Hello World prueba\n');
			})
			.post('/',upload.single('image'),	(req, res) => {
        let shell = require('shelljs');
				try {
          const f = req.file;
          console.log(f.destination + '/' + f.filename);
          shell.cd('/home/centos');
          let pylog = shell.exec('python testnodejs.py');
          //const {stdout, stderr} = shelljs.exec('ping www.google.com', {silent: true, async: true})
          console.log(pylog);
          //console.log(typeof pythonlog);
					res.sendStatus(200);
				} catch (err) {
					console.error("Error %s", err);
					res.sendStatus(400);
				}
			});
		});
		return router;
}
