import express from 'express';
import config from './lib/config';
import setup from './lib/setup';
import model from './lib/models';

let app = express();
//let db = model(config);
app['db'] = model(config);
setup(app/*, db*/);

app.db.sequelize
	.sync().then(() => {
		app.listen(config.port, () => {
			console.log('Express server listening on port ' + config.port);
		});
	}).catch((err) => {
		throw new Error(err);
	});
process.on('SIGINT', function() {
	app.db.sequelize.close();
	process.exit();
});
