# osiris

## Como enviar un csv

```
$ curl -F "csvdata=@rssicsv.csv" http://localhost:3000/api/rssi
{"originalName":"rssicsv.csv","encoding":"7bit","mimetype":"application/octet-stream","size":120}
```

```
curl -F "csvdata=@rssicsv.csv;type=text/csv" http://localhost:3000/api/rssi
{"originalName":"rssicsv.csv","encoding":"7bit","mimetype":"text/csv","size":120}
```
