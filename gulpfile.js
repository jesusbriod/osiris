const gulp = require('gulp');
const gutil = require('gulp-util');
const argv = require('minimist')(process.argv);
const babel = require('gulp-babel');
const gulpif = require('gulp-if');
const prompt = require('gulp-prompt');
const rsync = require('gulp-rsync');
const eslint = require('gulp-eslint');

const BUILD = 'build'
const rsyncPaths = [BUILD, 'package.json', 'process.yml'];
const urlpat = /(\w+)@([\w.-]+):([\w\/]+)/g;

gulp.task('lint', function() {
	return gulp.src('src/**/*.js')
		.pipe(eslint())
		.pipe(eslint.format())
		.pipe(eslint.failAfterError());
});

gulp.task('transpiler', ['lint'], () => {
	return gulp.src('src/**/*.js')
		.pipe(babel({
			presets: ['env']
		}))
		.pipe(gulp.dest(BUILD));
});

gulp.task('deploy', ['transpiler'], () => {
	let rsyncConf = {
		progress: true,
		incremental: true,
		relative: true,
		emptyDirectories: true,
		recursive: true,
		clean: true,
		compress: true,
		exclude: [],
	};

	let url = argv.to.split(/\@|:/);
	rsyncConf.username = url[0];
	rsyncConf.hostname = url[1];
	rsyncConf.destination = url[2];
	return gulp.src(rsyncPaths)
		.pipe(rsync(rsyncConf));
});

gulp.task('default', ['transpiler']);
